import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StoresComponent } from './components/stores/stores.component';
import { HelpComponent } from './components/help/help.component';
import { StoreComponent } from './components/stores/store/store.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/stores' },
  { path: 'stores',
    children: [
      { path: '', component: StoresComponent },
      { path: ':id', component: StoreComponent}
    ]
  },
  { path: 'help', component: HelpComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
