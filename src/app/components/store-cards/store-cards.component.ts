import { Component, Input, OnInit } from '@angular/core';
import { StoreModel } from '../../models/store.model';

@Component({
  selector: 'nc-store-cards',
  templateUrl: './store-cards.component.html',
  styleUrls: ['./store-cards.component.scss']
})
export class StoreCardsComponent implements OnInit {

  @Input()
  stores: Array<StoreModel> = [];

  @Input()
  withEmpty = true;

  constructor() { }

  ngOnInit(): void {
  }

  /**
   * Vérifier si au moins un commerce propose des paniers si l'option withEmpty est false
   */
  checkStoresCapacity(): boolean {
    return this.withEmpty
        || this.stores.filter(s => s.capacity > 0).length > 0;
  }
}
